package code

import (
	"github.com/bilibili/kratos/pkg/ecode"
)

// error code
var (
	ServerErr            = ecode.New(500)
	InvalidRequestParams = ecode.New(400000)
)
