package code

import (
	"github.com/bilibili/kratos/pkg/ecode"
)

// request
var (
	ErrInvalidRequestParams = ecode.Error(InvalidRequestParams, "InvalidRequestParams") // ErrInvalidRequestParams
)

// ErrInvalidRequestParam .
func ErrInvalidRequestParam(msg string) *ecode.Status {
	return ecode.Error(InvalidRequestParams, msg)
}

// default
var (
	ErrServerErr = ecode.Error(ServerErr, "ServerErr") // ErrServerErr
)
