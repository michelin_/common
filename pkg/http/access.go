package http

import (
	corehttp "net/http"
)

// AccessControl .
func AccessControl(h corehttp.Handler) corehttp.Handler {
	return corehttp.HandlerFunc(func(w corehttp.ResponseWriter, r *corehttp.Request) {
		w.Header().Set("Content-Type", "application/hal+json")
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Credentials", "true")
		w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, PATCH, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Length, Content-Type, Timestamp, Token, Version, X-Requested-With, Authorization")
		if r.Method == "OPTIONS" {
			return
		}

		h.ServeHTTP(w, r)
	})
}
