package http

import (
	"bytes"
	"encoding/json"
	"gitlab.com/michelin_/common/pkg/log"
	"go.uber.org/zap"
	"io"
	"io/ioutil"
	"net/http"
	"time"
)

// Get 发送GET请求
// url:请求地址
// response:请求返回的内容
func Get(url string) (res string, err error) {
	client := http.Client{Timeout: 5 * time.Second}
	resp, err := client.Get(url)
	if err != nil {
		log.Error("Get:client.Get err", zap.String("url", url), zap.String("err", err.Error()))
		return "", err
	}
	defer resp.Body.Close()
	var buffer [512]byte
	result := bytes.NewBuffer(nil)
	for {
		n, err := resp.Body.Read(buffer[0:])
		result.Write(buffer[0:n])
		if err != nil && err == io.EOF {
			break
		} else if err != nil {
			log.Error("Get:result.Write err", zap.String("url", url), zap.String("err", err.Error()))
			return "", err
		}
	}

	return result.String(), nil
}

// Post 发送POST请求
// url:请求地址，data:POST请求提交的数据,contentType:请求体格式，如：application/json
// content:请求放回的内容
func Post(url string, data interface{}, contentType string) (res string, err error) {
	jsonStr, _ := json.Marshal(data)
	req, err := http.NewRequest(`POST`, url, bytes.NewBuffer(jsonStr))
	req.Header.Add(`content-type`, contentType)
	if err != nil {
		log.Error("Post:http.NewRequest err", zap.String("url", url), zap.String("err", err.Error()))
		return "", err
	}
	defer req.Body.Close()

	client := &http.Client{Timeout: 5 * time.Second}
	resp, err := client.Do(req)
	if err != nil {
		log.Error("Post:client.Do err", zap.String("url", url), zap.String("err", err.Error()))
		return "", err
	}
	defer resp.Body.Close()

	result, _ := ioutil.ReadAll(resp.Body)
	return string(result), nil
}
