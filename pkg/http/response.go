package http

import (
	"context"
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/bilibili/kratos/pkg/ecode"
	"gitlab.com/michelin_/common/pkg/code"
)

// ------------------------------------------------------------------------------

// EncodeResponse .
func EncodeResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	writeStatusCode(w, 0)
	writeContentType(w, jsonContentType)

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, PATCH, OPTIONS")
	w.Header().Set("Access-Control-Allow-Headers", "Origin, Content-Length, Content-Type")
	return json.NewEncoder(w).Encode(map[string]interface{}{
		"code": 0,
		"msg":  "success",
		"data": response,
	})
}

// EncodeError .
func EncodeError(_ context.Context, err error, w http.ResponseWriter) {
	bcode := ecode.Cause(err)
	switch err {
	case code.ErrServerErr:
		w.WriteHeader(http.StatusInternalServerError)
	// 指定了错误的不返回 500
	// 没找到错误码的默认返回 500
	default:
		if bcode.Code() == -500 {
			bcode = code.ErrServerErr
			w.WriteHeader(http.StatusInternalServerError)
		}
	}

	writeStatusCode(w, bcode.Code())
	writeContentType(w, jsonContentType)

	json.NewEncoder(w).Encode(map[string]interface{}{
		"code": bcode.Code(),
		"msg":  bcode.Message(),
	})
}

var jsonContentType = []string{"application/json; charset=utf-8"}

func writeStatusCode(w http.ResponseWriter, ecode int) {
	header := w.Header()
	header.Set("ngx-status-code", strconv.FormatInt(int64(ecode), 10))
}

func writeContentType(w http.ResponseWriter, value []string) {
	header := w.Header()
	if val := header["Content-Type"]; len(val) == 0 {
		header["Content-Type"] = value
	}
}
