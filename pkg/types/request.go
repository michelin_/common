package types

import (
	"mime/multipart"
	"net/http"
	"strconv"

	ccode "gitlab.com/michelin_/common/pkg/code"
	"gitlab.com/michelin_/common/pkg/log"
	"go.uber.org/zap"
)

// PaginationParams Get pagination params.
func PaginationParams(r *http.Request) (sort, direction string, page, limit int, err error) {
	sort = r.FormValue("sort")
	direction = r.FormValue("direction")
	p := r.FormValue("page")
	l := r.FormValue("limit")
	page, err = strconv.Atoi(p)
	if err != nil {
		err = ccode.ErrInvalidRequestParam("Invalid page")
		return
	}
	limit, err = strconv.Atoi(l)
	if err != nil {
		err = ccode.ErrInvalidRequestParam("Invalid limit")
		return
	}

	if page == 0 || limit == 0 {
		err = ccode.ErrInvalidRequestParams
		return
	}
	return
}

// ------------------------------------------------------------

// UploadImgRequest .
type UploadImgRequest struct {
	File       multipart.File
	FileHeader *multipart.FileHeader
}

// NewUploadImgRequest .
func NewUploadImgRequest(r *http.Request) (*UploadImgRequest, error) {
	file, fileHeader, err := r.FormFile("file")
	if err != nil {
		log.Error("r.FormFile err", zap.String("method", "NewUploadImgRequest"), zap.String("err", err.Error()))
		return nil, ccode.ErrInvalidRequestParam(err.Error())
	}
	defer file.Close()
	return &UploadImgRequest{
		File:       file,
		FileHeader: fileHeader,
	}, nil
}
