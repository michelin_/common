package viper

import (
	"log"
	"os"

	rawViper "github.com/noprom/viper"
	_ "github.com/noprom/viper/remote"
)

// NewViper create a viper from file.
func NewViper(cfg string) (*rawViper.Viper, error) {
	var cfgViper = rawViper.New()
	if cfgfile, err := os.Open(cfg); err != nil {
		return nil, err
	} else {
		cfgViper.SetConfigType("yaml")
		if err := cfgViper.ReadConfig(cfgfile); err != nil {
			return nil, err
		}
		return cfgViper, nil
	}
}

// NewConsulViper create a remote consul viper.
func NewConsulViper(consulAddr, consulPath string) *rawViper.Viper {
	var runtimeViper = rawViper.New()
	if consulAddr != "" && consulPath != "" {
		// runtimeViper.AddSecureRemoteProvider("consul", consulAddr, consulConfigPath, secretKeyRing)
		runtimeViper.AddRemoteProvider("consul", consulAddr, consulPath)
		runtimeViper.SetConfigType("yaml")
		if err := runtimeViper.ReadRemoteConfig(); err != nil {
			log.Printf("Can't read consul config:: %v", err)
			os.Exit(1)
		}
		// open a goroutine to watch remote changes forever
		// go func() {
		// 	for {
		// 		time.Sleep(time.Second * 5) // delay after each request

		// 		if err := runtimeViper.WatchRemoteConfig(); err != nil {
		// 			log.Printf("Can't read consul config after changes:: %v", err)
		// 			continue
		// 		}
		// 	}
		// }()
	}
	// Test config get
	version := runtimeViper.GetString("version")
	log.Print("Load config version: ", version)

	return runtimeViper
}
